Turma de Mestrado 2013 - Coppead ================================

Cliente
-------

Mestrado Coppead

Objetivos
---------

- Repensar o site atual do curso, com base na identidade visual nova da
Coppead, dando foco nos filtros de busca por aluno/áreas de interesse. -
Facilitar a estrutura de catalogação dos alunos, criando um ambiente
unificado a ser utilizado também pelas próximas turmas.

Escopo
------

### Direção de Arte

### Implementação

Templates de página com SEO, técnicas modernas de efeitos visuais nas
respostas às ações dos usuários e bom funcionamento em dispositivos
móveis.

### Programação

Integração de todo o site a um sistema de gerenciamento de
conteúdo(CMS), onde será possível cadastrar e editar perfis, áreas,
setores, anos e selecionar quais perfis destacar na home.

Responsáveis
------------

- Arquitetura / Direção de arte: Ítalo Monteiro - Programação /
Implementação: Felipe Castro

Sitemap
-------

Home

Turma

-   Principal
-   Perfil narrativo/ Perfil curricular
-   Resultado de busca

Mestrado

-   Áreas de interesse
-   Experiência Multicultural
-   Programa de Intercâmbio
-   Cátedras
-   Extra-classe

Coppead

-   Sobre a coppead
-   Depoimentos

Contato - formulário

Cronograma
----------

**12 de Abril** - Depósito no valor de 50% \> Direção de Arte - Layout
\> Home / Identidade Visual **13 e 14 de Abril** - Aprovação \> Home
**17 de Abril** - Entrega \> Layout \> Turma \> Principal \> Perfil
narrativo/ Perfil curricular \> Resultado de busca **19 de Abril** -
Entrega \> Layout \> Mestrado \> Áreas de interesse \> Experiência
Multicultural / Programa de Intercâmbio \> Cátedras \> Extra-classe **20
e 21 de Abril** - Aprovação \> Layout \> Turma \> Layout \> Mestrado
**22 de Abril** - Entrega \> Layout \> Coppead \> Sobre a coppead \>
Depoimentos - Entrega \> Layout \> Contato **23 a 26 de Abril** -
Depósito dos 50% restantes \> Direção de Arte - Ajustes \> Layout **27
de Abril a 17 de Maio (21 dias)**\
 **02 de Maio a 22 Maio \<-- Reajustado** - Depósito no valor de 50% \>
Programação/Implementação - Implementação **20 de Maio a 24 de Maio (4
dias)**\
 **25 de Maio a 29 de Maio \<-- Reajustado** - Programação **27 a 30 de
Maio (3 dias)**\
 **01 a 04 de Junho \<-- Reajustado** - Ajustes e Testes **31 de Maio**\
 **07 de Junho \<-- Reajustado** - Entrega do Site finalizado - Depósito
dos 50% restantes \> Programação/Implementação: R\$ 1.300 \* O
cronograma irá caminhar de acordo com a necessidade de ajuste em cada
entrega. Esse tempo contempla apenas a versão em 'português' do site.
Para a versão em 'inglês' as páginas precisarão ser layoutadas e
implementadas novamente, o que gerará em um novo cronograma, e um novo
custo de produção. Isso será feito posteriormente a entrega da versão em
'português'.

#### Observações

Site atual do Mestrado Coppead: http://mestradocoppead.com.br/

#### Referências

**Interface** - Harvard - http://www.hbs.edu/about/Pages/default.aspx
**Filtros** - Harvard -
http://www.hbs.edu/mba/people/Pages/default.aspx?type=Students - EA -
http://www.ea.com/games - A idéia é usarmos navegação multifacetada
(assim como nas referências acima) com as seguintes facetas: área, setor
e ano.